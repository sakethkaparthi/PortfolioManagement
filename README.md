**Add Stock Price**
----
  Adds the stock price for a date into the database

* **URL**

  /api/stockprice/create

* **Method:**

  `POST`
  
* **Body**

    {
    "stockDate": "2021-05-31",
    "stockName": "XXXX",
    "stockPrice": 44.00
    }

* **Success Response:**

  * **Code:** 200
    **Content:** `{
    "stockDate": "2021-05-31",
    "stockName": "XXXX",
    "stockPrice": 44.00
    }`
 


**Search Stock Price**
----
  Returns the stock price wrt to the given date

* **URL**

  /api/stockprice/search

* **Method:**

  `POST`
  
* **Body**

    {
    "stockDate": "2021-05-31",
    "stockName": "XXXX",
    }

* **Success Response:**

  * **Code:** 200 
    **Content:** `{
    "stockDate": "2021-05-31",
    "stockName": "XXXX",
    "stockPrice": 44.00
    }`
 

 **Add Trade**
----
  Add a trade into the database

* **URL**

  /api/trade/

* **Method:**

  `POST`

* **Body**
    {
    "type" : "SELL",
    "date": "2021-01-14",
    "stockName": "YYYY",
    "quantity": 200
    }

* **Success Response:**

  * **Code:** 200
    **Content:** `{
    "id": 21,
    "type": "BUY",
    "date": "2021-01-14",
    "quantity": 200,
    "stockName": "YYYY",
    "stockPrice": 300.34
}`
 

**Get all executed trades**
----
  Returns list of all trades

* **URL**

  /api/trade/

* **Method:**

  `GET`

* **Body**
    
    None

* **Success Response:**

  * **Code:** 200
    **Content:** `[
    {
        "id": 16,
        "type": "BUY",
        "date": "2021-01-13",
        "quantity": 200,
        "stockName": "YYYY",
        "stockPrice": 298.33
    },
    {
        "id": 17,
        "type": "SELL",
        "date": "2021-01-14",
        "quantity": 300,
        "stockName": "YYYY",
        "stockPrice": 300.34
    }]`


**Update trade**
----
  Update trade in database

* **URL**

  /api/trade/:id

* **Method:**

  `PUT`

* **Body**
    
    {
    "type" : "BUY",
    "date": "2021-01-14",
    "stockName": "YYYY",
    "quantity": 310
    }

* **Success Response:**

  * **Code:** 200
    **Content:** `{
    "id": 21,
    "type": "BUY",
    "date": "2021-01-14",
    "quantity": 310,
    "stockName": "YYYY",
    "stockPrice": 300.34
}`


**Get portfolio**
----
  Get the portfolio as of current date

* **URL**

  /api/portfolio

* **Method:**

  `GET`

* **Body**
    
    `None`

* **Success Response:**

  * **Code:** 200
    **Content:** `[
    {
        "stockName": "YYYY",
        "quantity": 300,
        "averageBuyPrice": 295.6,
        "currentPrice": 295.6,
        "marketValue": 88679.0,
        "unrealizedReturns": 0,
        "realizedReturns": 1422.9
    },
    {
        "stockName": "XXXX",
        "quantity": 700,
        "averageBuyPrice": 42.54,
        "currentPrice": 44.0,
        "marketValue": 29778.0,
        "unrealizedReturns": 1022.0,
        "realizedReturns": 0.0
    }]`

 
