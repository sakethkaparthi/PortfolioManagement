package com.saketh.PortfolioManagement.services;

import com.saketh.PortfolioManagement.dtos.StockPriceDto;

public interface StockPriceService {
    StockPriceDto getStockPrice(String stockName, String datetime);

	StockPriceDto createStockPrice(StockPriceDto stockPrice);
}
