package com.saketh.PortfolioManagement.services;

import java.util.List;

import com.saketh.PortfolioManagement.dtos.PortfolioDto;

public interface PortfolioService {
	List<PortfolioDto> getPortfolio();
}
