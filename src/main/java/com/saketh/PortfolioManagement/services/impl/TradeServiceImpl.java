package com.saketh.PortfolioManagement.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import com.saketh.PortfolioManagement.dtos.TradeDto;
import com.saketh.PortfolioManagement.dtos.TradeRequestDto;
import com.saketh.PortfolioManagement.entities.TradeEntity;
import com.saketh.PortfolioManagement.repositories.StockPriceRepository;
import com.saketh.PortfolioManagement.repositories.TradeRepository;
import com.saketh.PortfolioManagement.services.TradeService;

@Transactional(value = TxType.REQUIRED)
@Service
public class TradeServiceImpl implements TradeService {
	private TradeRepository tradeRepository;
	private StockPriceRepository stockPriceRepository;

	public TradeServiceImpl(TradeRepository tradeRepository, StockPriceRepository stockPriceRepository) {
		super();
		this.tradeRepository = tradeRepository;
		this.stockPriceRepository = stockPriceRepository;
	}

	@Override
	public List<TradeDto> getAllTrades() {
		var allTradeEntities = tradeRepository.findAll();
		var allTradeDtos = new ArrayList<TradeDto>();
		allTradeEntities.forEach((e) -> allTradeDtos.add(new TradeDto(e)));
		allTradeDtos.forEach((e) -> e.setStockPrice(getStockPrice(e)));
		return allTradeDtos;
	}

	private Double getStockPrice(TradeDto dto) {
		return stockPriceRepository.findByNameAndDate(dto.getStockName(), dto.getDate()).getPrice();
	}

	@Override
	public TradeDto createTrade(TradeRequestDto trade) {
		TradeDto tradeObject =  new TradeDto(tradeRepository.save(trade.mapToEntity()));
		tradeObject.setStockPrice(getStockPrice(tradeObject));
		return tradeObject;
	}

	@Override
	public void deleteTrade(Long id) {
		tradeRepository.deleteById(id);
	}

	@Override
	public TradeDto updateTrade(Long id, TradeRequestDto trade) {
		TradeEntity existingEntity = tradeRepository.findById(id)
				.orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND));
		existingEntity.setDate(trade.getDate());
		existingEntity.setType(trade.getType());
		existingEntity.setStockName(trade.getStockName());
		existingEntity.setQuantity(trade.getQuantity());
		return new TradeDto(existingEntity);
	}

}
