package com.saketh.PortfolioManagement.services.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.saketh.PortfolioManagement.dtos.PortfolioDto;
import com.saketh.PortfolioManagement.dtos.TradeDto;
import com.saketh.PortfolioManagement.entities.TradeEntity.TradeType;
import com.saketh.PortfolioManagement.services.PortfolioService;
import com.saketh.PortfolioManagement.services.StockPriceService;
import com.saketh.PortfolioManagement.services.TradeService;

@Service
public class PortfolioServiceImpl implements PortfolioService {
	private TradeService tradeService;
	private StockPriceService stockPriceService;

	public PortfolioServiceImpl(TradeService tradeService, StockPriceService stockPriceService) {
		super();
		this.tradeService = tradeService;
		this.stockPriceService = stockPriceService;
	}

	@Override
	public List<PortfolioDto> getPortfolio() {
		Map<String, List<TradeDto>> allTrades = tradeService.getAllTrades().stream()
				.collect(Collectors.groupingBy(trade -> trade.getStockName()));
		return allTrades.entrySet().stream().map(e -> getPortfolio(e.getKey(), e.getValue()))
				.collect(Collectors.toList());
	}

	private PortfolioDto getPortfolio(String stockName, List<TradeDto> trades) {
		PortfolioDto portfolio = new PortfolioDto();
		portfolio.setStockName(stockName);
		portfolio.setCurrentPrice(stockPriceService
				.getStockPrice(stockName, LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
				.getStockPrice());
		portfolio.setQuantity(trades.stream()
				.map(trade -> trade.getType().equals(TradeType.BUY) ? trade.getQuantity() : -trade.getQuantity())
				.reduce(0, (acc, quantity) -> acc + quantity));
		
		Integer noOfBuyTrades = trades.stream().filter(trade -> trade.getType().equals(TradeType.BUY))
				.map(trade -> trade.getQuantity())
				.reduce(Integer::sum)
				.get();
		
		portfolio.setAverageBuyPrice(trades.stream()
				.filter(trade -> trade.getType().equals(TradeType.BUY))
				.map(trade -> trade.getStockPrice() * trade.getQuantity())
				.reduce(0.0, (acc, price) -> (acc + price))/noOfBuyTrades);
		
		portfolio.setMarketValue(portfolio.getQuantity() * portfolio.getAverageBuyPrice());
		
		portfolio.setUnrealizedReturns((portfolio.getCurrentPrice() - portfolio.getAverageBuyPrice()) * portfolio.getQuantity());
		
		Double sellAmount = trades.stream()
				.filter(trade -> trade.getType().equals(TradeType.SELL))
				.map(trade -> trade.getStockPrice() * trade.getQuantity())
				.reduce(Double::sum)
				.orElse(0.0);
		
		portfolio.setRealizedReturns(sellAmount == 0 ? 0 : sellAmount - (portfolio.getAverageBuyPrice() * portfolio.getQuantity()));
		
		return portfolio;
	}
}
