package com.saketh.PortfolioManagement.services.impl;

import org.springframework.stereotype.Service;

import com.saketh.PortfolioManagement.dtos.StockPriceDto;
import com.saketh.PortfolioManagement.repositories.StockPriceRepository;
import com.saketh.PortfolioManagement.services.StockPriceService;

@Service
public class StockPriceServiceImpl implements StockPriceService {
	private StockPriceRepository stockPriceRepository;

	public StockPriceServiceImpl(StockPriceRepository stockPriceRepository) {
		super();
		this.stockPriceRepository = stockPriceRepository;
	}

	@Override
	public StockPriceDto getStockPrice(String stockName, String date) {
		validateDate(date);
		return new StockPriceDto(stockPriceRepository.findByNameAndDate(stockName, date));
	}

	@Override
	public StockPriceDto createStockPrice(StockPriceDto stockPrice) {
		validateDate(stockPrice.getStockDate());
		return new StockPriceDto(stockPriceRepository.save(stockPrice.mapToEntity()));
	}

	private void validateDate(String date) {
		String[] dateParts = date.split("-");
		if (dateParts.length != 3)
			throw new RuntimeException("Invalid date format. Expecting yyyy-mm-dd");
		int month = Integer.parseInt(dateParts[1]);
		int day = Integer.parseInt(dateParts[2]);
		if (month < 1 || month > 12 || day < 1 || day > 31)
			throw new RuntimeException("Invalid date format. Expecting yyyy-mm-dd");
	}
}
