package com.saketh.PortfolioManagement.services;

import java.util.List;

import com.saketh.PortfolioManagement.dtos.TradeDto;
import com.saketh.PortfolioManagement.dtos.TradeRequestDto;

public interface TradeService {
	List<TradeDto> getAllTrades();

	TradeDto createTrade(TradeRequestDto trade);
	
	void deleteTrade(Long id);
	
	TradeDto updateTrade(Long id, TradeRequestDto trade);
}
