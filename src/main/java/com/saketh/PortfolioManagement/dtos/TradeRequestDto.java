package com.saketh.PortfolioManagement.dtos;

import com.saketh.PortfolioManagement.entities.TradeEntity;
import com.saketh.PortfolioManagement.entities.TradeEntity.TradeType;

public class TradeRequestDto {
	private Long id;
	private TradeType type;
	private String date;
	private Integer quantity;
	private String stockName;

	public TradeRequestDto() {
	}

	public TradeRequestDto(Long id, TradeType type, String date, Integer quantity, String stockName) {
		super();
		this.id = id;
		this.type = type;
		this.date = date;
		this.quantity = quantity;
		this.stockName = stockName;
	}

	public TradeRequestDto(TradeEntity tradeEntity) {
		this.id = tradeEntity.id;
		this.type = tradeEntity.getType();
		this.date = tradeEntity.getDate();
		this.quantity = tradeEntity.getQuantity();
		this.stockName = tradeEntity.getStockName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TradeType getType() {
		return type;
	}

	public void setType(TradeType type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public TradeEntity mapToEntity() {
		return new TradeEntity(type, date, quantity, stockName);
	}
}
