package com.saketh.PortfolioManagement.dtos;

public class PortfolioDto {
	private String stockName;
	private Integer quantity;
	private Double averageBuyPrice;
	private Double currentPrice;
	private Double marketValue;
	private Double unrealizedReturns;
	private Double realizedReturns;

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getAverageBuyPrice() {
		return averageBuyPrice;
	}

	public void setAverageBuyPrice(Double averageBuyPrice) {
		this.averageBuyPrice = averageBuyPrice;
	}

	public Double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public Double getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(Double marketValue) {
		this.marketValue = marketValue;
	}

	public Double getUnrealizedReturns() {
		return unrealizedReturns;
	}

	public void setUnrealizedReturns(Double unrealizedReturns) {
		this.unrealizedReturns = unrealizedReturns;
	}

	public Double getRealizedReturns() {
		return realizedReturns;
	}

	public void setRealizedReturns(Double realizedReturns) {
		this.realizedReturns = realizedReturns;
	}
}
