package com.saketh.PortfolioManagement.dtos;

public class StockPriceRequestDto {
	private String stockName;
	private String stockDate;

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockDate() {
		return stockDate;
	}

	public void setStockDate(String stockDate) {
		this.stockDate = stockDate;
	}
}
