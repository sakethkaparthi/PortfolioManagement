package com.saketh.PortfolioManagement.dtos;

import com.saketh.PortfolioManagement.entities.StockPriceEntity;

public class StockPriceDto {
	private String stockName;
	private String stockDate;
	private Double stockPrice;

	public StockPriceDto() {
		
	}
	
	public StockPriceDto(String stockName, String stockDate, Double stockPrice) {
		this.stockName = stockName;
		this.stockDate = stockDate;
		this.stockPrice = stockPrice;
	}

	public StockPriceDto(StockPriceEntity stockPriceEntity) {
		this.stockName = stockPriceEntity.getName();
		this.stockDate = stockPriceEntity.getDate();
		this.stockPrice = stockPriceEntity.getPrice();
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockDate() {
		return stockDate;
	}

	public void setStockDate(String stockDate) {
		this.stockDate = stockDate;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public StockPriceEntity mapToEntity() {
		return new StockPriceEntity(stockName, stockPrice, stockDate);
	}
}