package com.saketh.PortfolioManagement.dtos;

import com.saketh.PortfolioManagement.entities.TradeEntity;
import com.saketh.PortfolioManagement.entities.TradeEntity.TradeType;

public class TradeDto {
	private Long id;
	private TradeType type;
	private String date;
	private Integer quantity;
	private String stockName;
	private Double stockPrice;

	public TradeDto() {
	}

	public TradeDto(Long id, TradeType type, String date, Integer quantity, String stockName, Double price) {
		super();
		this.id = id;
		this.type = type;
		this.date = date;
		this.quantity = quantity;
		this.stockName = stockName;
		this.stockPrice = price;
	}

	public TradeDto(TradeEntity tradeEntity) {
		this.id = tradeEntity.id;
		this.type = tradeEntity.getType();
		this.date = tradeEntity.getDate();
		this.quantity = tradeEntity.getQuantity();
		this.stockName = tradeEntity.getStockName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TradeType getType() {
		return type;
	}

	public void setType(TradeType type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public TradeEntity mapToEntity() {
		return new TradeEntity(type, date, quantity, stockName);
	}
}
