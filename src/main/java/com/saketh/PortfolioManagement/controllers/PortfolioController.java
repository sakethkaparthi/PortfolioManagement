package com.saketh.PortfolioManagement.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saketh.PortfolioManagement.dtos.PortfolioDto;
import com.saketh.PortfolioManagement.services.PortfolioService;

@RestController
public class PortfolioController {
	private PortfolioService portfolioService;

	public PortfolioController(PortfolioService portfolioService) {
		this.portfolioService = portfolioService;
	}
	
	@GetMapping("/api/portfolio")
	public List<PortfolioDto> getPortfolio(){
		return portfolioService.getPortfolio();
	}
}