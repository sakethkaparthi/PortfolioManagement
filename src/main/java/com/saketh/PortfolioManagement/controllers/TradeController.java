package com.saketh.PortfolioManagement.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.saketh.PortfolioManagement.dtos.TradeDto;
import com.saketh.PortfolioManagement.dtos.TradeRequestDto;
import com.saketh.PortfolioManagement.services.TradeService;

@RestController
public class TradeController {
	private TradeService tradeService;

	public TradeController(TradeService tradeService) {
		super();
		this.tradeService = tradeService;
	}

	@GetMapping(value = "/api/trade/")
	public List<TradeDto> getAllTrades() {
		return tradeService.getAllTrades();
	}

	@PostMapping(value = "/api/trade/")
	public TradeDto addTrade(@RequestBody TradeRequestDto trade) {
		return tradeService.createTrade(trade);
	}
	
	@DeleteMapping(value = "/api/trade/{id}")
	public void deleteTrade(@PathVariable Long id) {
		tradeService.deleteTrade(id);
	}
	
	@PutMapping(value = "/api/trade/{id}")
	public void updateTrade(@PathVariable Long id, @RequestBody TradeRequestDto trade) {
		
	}
}
