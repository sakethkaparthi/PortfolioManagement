package com.saketh.PortfolioManagement.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.saketh.PortfolioManagement.dtos.StockPriceDto;
import com.saketh.PortfolioManagement.dtos.StockPriceRequestDto;
import com.saketh.PortfolioManagement.services.StockPriceService;

@RestController
public class StockPriceController {
	private StockPriceService stockPriceService;	
	
	public StockPriceController(StockPriceService stockPriceService) {
		super();
		this.stockPriceService = stockPriceService;
	}

	@PostMapping(value = "/api/stockprice/search")
	public StockPriceDto getStockPrice(@RequestBody StockPriceRequestDto stockPriceRequest) {
		return stockPriceService.getStockPrice(stockPriceRequest.getStockName(), stockPriceRequest.getStockDate());
	}
	
	@PostMapping(value = "/api/stockprice/create")
	public StockPriceDto createStockPrice(@RequestBody StockPriceDto stockPrice) {
		return stockPriceService.createStockPrice(stockPrice);
	}
}
