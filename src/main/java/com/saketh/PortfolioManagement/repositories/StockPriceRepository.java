package com.saketh.PortfolioManagement.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saketh.PortfolioManagement.entities.StockPriceEntity;

@Repository
public interface StockPriceRepository  extends CrudRepository<StockPriceEntity, Long>{
	public StockPriceEntity findByNameAndDate(String name, String date);
}
