package com.saketh.PortfolioManagement.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.saketh.PortfolioManagement.entities.TradeEntity;

@Repository
public interface TradeRepository extends CrudRepository<TradeEntity, Long> {
	List<TradeEntity> findAll();
}
