package com.saketh.PortfolioManagement.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "trade")
public class TradeEntity {
	public enum TradeType {
		BUY, SELL
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	@Column
	@Enumerated(value = EnumType.STRING)
	private TradeType type;
	@Column(name = "stock_date")
	private String date;
	@Column
	private Integer quantity;
	@Column(name = "stock_name")
	private String stockName;

	public TradeEntity() {
		
	}
	
	public TradeEntity(TradeType type, String date, Integer quantity, String stockName) {
		super();
		this.type = type;
		this.date = date;
		this.quantity = quantity;
		this.stockName = stockName;
	}

	public String getDate() {
		return date;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public String getStockName() {
		return stockName;
	}

	public TradeType getType() {
		return type;
	}

	public void setType(TradeType type) {
		this.type = type;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	@Override
	public String toString() {
		return String.format("Trade[id=%s, stock name='%s', trade type='%s', quantity='%s']", id, stockName, type,
				quantity);
	}
}
