package com.saketh.PortfolioManagement.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "stock_price")
public class StockPriceEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "stock_name")
    private String name;
	@Column
    private Double price;
	@Column(name = "stock_date")
    private String date;

	public StockPriceEntity() {
		
	}
	
    public StockPriceEntity(String name, Double price, String date) {
        this.name = name;
        this.price = price;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}